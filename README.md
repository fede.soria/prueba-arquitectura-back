## Pasos para levantar mysql en la VM.

Creamos la red a través de la cual se van a comunicar todos los contenedores

    docker network create -d bridge network_prueba_arquitectura
Creamos el volúmen para la persistencia de datos mysql

    docker volume create mysql-db-data
    
Creamos el contenedor mysql, con la network y el volúmen creados en los pasos anteriores
    
    docker run --name mysql --network network_prueba_arquitectura --mount src=mysql-db-data,dst=/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -d mysql --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
