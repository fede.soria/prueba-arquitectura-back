FROM node:12-buster-slim
RUN npm install pm2 -g
WORKDIR /prueba-arquitectura-back-ms
COPY package.json ./package.json
RUN npm install
COPY . ./
COPY ormconfig-prod.json ./ormconfig.json
RUN npm run build
RUN pm2 dump
EXPOSE 3102
CMD [ "pm2-runtime", "npm", "--", "start" ]
