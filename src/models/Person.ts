import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, } from "typeorm";
import { Role } from './Role';

@Index("fk_person_role_idx", ["role"], {})
@Entity("person", {schema: "prueba-arquitectura"})
export class Person {
    @PrimaryGeneratedColumn({type: "int", name: "id"})
    id: number;

    @Column("varchar", {name: "firstname", nullable: true, length: 45})
    firstname: string | null;

    @Column("varchar", {name: "lastname", nullable: true, length: 45})
    lastname: string | null;

    @Column("int", {name: "role", nullable: true})
    role: number | null;

    @ManyToOne(() => Role, (role) => role.people, {
        onDelete: "NO ACTION",
        onUpdate: "NO ACTION",
    })
    @JoinColumn([{name: "role", referencedColumnName: "id"}])
    role2: Role;
}
