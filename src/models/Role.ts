import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Person } from './Person';

@Entity("role", { schema: "prueba-arquitectura" })
export class Role {
    @PrimaryGeneratedColumn({ type: "int", name: "id" })
    id: number;

    @Column("varchar", { name: "name", nullable: true, length: 45 })
    name: string | null;

    @OneToMany(() => Person, (person) => person.role2)
    people: Person[];
}
