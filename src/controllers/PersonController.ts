import * as express from 'express';
import { getManager } from 'typeorm';
import { Person } from '../models/Person';

class PersonController {
    public path = '/person';
    public router: express.Router = express.Router();

    constructor() {
        this.initializeRoutes();
    }

    public initializeRoutes() {
        this.router.get(this.path + '/:idRole', this.getPersonsByRole);
    }

    public getPersonsByRole(idRole: number) {
        console.log('entro con' + idRole);
        return getManager().getRepository(Person).find({
            where: {
                role: idRole
            }
        })
    }
}

export default PersonController;
