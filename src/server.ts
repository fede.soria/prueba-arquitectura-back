import * as express from 'express';
import PersonController from './controllers/PersonController';
import { createConnection } from 'typeorm';
import "dotenv/config";

const personController = new PersonController();
const cors = require('cors');

createConnection().then((connection) => { console.log('connected')}).catch(error => console.log(error));

const app = express();
app.use(express.json());
app.use(cors());

app.get('/person/GetPersonsByRole/:idRole', (req, res) => {
    personController.getPersonsByRole(+req.params.idRole).then((result) => res.send(result));
})

const startServer = async () => {
    await app.listen(process.env.PORT || 3102, () => {
        console.log(
            'Server running on http://127.0.0.1:${process.env.PORT}'
            );
    });
};


(async () => {
    await startServer();
})();
