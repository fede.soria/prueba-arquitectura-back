"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var typeorm_1 = require("typeorm");
var Person_1 = require("../models/Person");
var PersonController = /** @class */ (function () {
    function PersonController() {
        this.path = '/person';
        this.router = express.Router();
        this.initializeRoutes();
    }
    PersonController.prototype.initializeRoutes = function () {
        this.router.get(this.path + '/:idRole', this.getPersonsByRole);
    };
    PersonController.prototype.getPersonsByRole = function (idRole) {
        return typeorm_1.getManager().getRepository(Person_1.Person).find({
            where: {
                role: idRole
            }
        });
    };
    return PersonController;
}());
exports.default = PersonController;
//# sourceMappingURL=PersonController.js.map