"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Person = void 0;
var typeorm_1 = require("typeorm");
var Role_1 = require("./Role");
var Person = /** @class */ (function () {
    function Person() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "id" }),
        __metadata("design:type", Number)
    ], Person.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "firstname", nullable: true, length: 45 }),
        __metadata("design:type", String)
    ], Person.prototype, "firstname", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "lastname", nullable: true, length: 45 }),
        __metadata("design:type", String)
    ], Person.prototype, "lastname", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "role", nullable: true }),
        __metadata("design:type", Number)
    ], Person.prototype, "role", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Role_1.Role; }, function (role) { return role.people; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "role", referencedColumnName: "id" }]),
        __metadata("design:type", Role_1.Role)
    ], Person.prototype, "role2", void 0);
    Person = __decorate([
        typeorm_1.Index("fk_person_role_idx", ["role"], {}),
        typeorm_1.Entity("person", { schema: "prueba-arquitectura" })
    ], Person);
    return Person;
}());
exports.Person = Person;
//# sourceMappingURL=Person.js.map